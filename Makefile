run_all_in_parallel:
	make -j test_Windows_10_edge_18 test_Windows_10_IE

test_Windows_10_chrome_80:
	robot -d results --variable platform:${LT_PLATFORM} --variable browserName:${LT_BROWSER_NAME} --variable version:${LT_BROWSER_VERSION} --variable resolution:${LT_RESOLUTION} --variable ROBOT_BROWSER:${LT_BROWSER_NAME} --variable visual:true --variable network:true --variable console:false --variable ENVIRONMENT:live tests

test_Windows_10_chrome_80_LOCAL:
	robot -d results --variable platform:"win10" --variable browserName:"Chrome" --variable version:"80" --variable resolution:"1920x1080" --variable ROBOT_BROWSER:"Chrome" --variable visual:true --variable network:true --variable console:false --variable ENVIRONMENT:live tests
