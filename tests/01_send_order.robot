*** Settings ***
Documentation     Test suite with Send order flow.
...
...               This test has a workflow that is created using keywords in
...               the imported resource_dstore.robot file.

Suite Setup       Open Browser To Homepage
Suite Teardown    Close All Browsers

Resource         ../resources/resource_dstore.robot
Resource         ../resources/variables_dstore.robot

#Command:
#robot -d results -v ENVIRONMENT:live -v BROWSER:chrome tests\01_send_order.robot

*** Test Cases ***
Customer should be able to successfully login
    Open Login Page
    Complete With Valid Dates And Submit User Login

Customer should be able to add a product to cart from the details page
    Select A Category and Random SubCategory
    Select A Product From Browse And Choose Variations

Customer should be able to search a product and add to cart
    Select A Category and Random SubCategory
    Search A Product And Use Autocomplete

Customer should be able to search a subcategory and add a product to cart
    Select A Random Subcategory
    Search The Name Of Subcategory And Add A Product To Cart

Customer should be able to go to the cart page and check the products
    Go To Cart
    Check The Price For Products
    Submit Cart to Checkout

Customer should be able to select an address, shipping and payment and submit step
    Select Current Address
    Select Shipping Method
    Select Payment and submit checkout

Customer should be able to complete the card details
    Complete all card details in payment page



