*** Settings ***
Library  LambdaTestStatus.py

*** Variables ***
@{_tmp}
    ...  platform: ${platform},
    ...  browserName: ${browserName},
    ...  version: ${version},
    ...  ENVIRONMENT: ${ENVIRONMENT},
    ...  resolution: ${resolution},
#    ...  takesScreenshot: ${takesScreenshot},
#    ...  hasTouchScreen: ${hasTouchScreen},
    ...  visual: ${visual},
    ...  network: ${network},
    ...  console: ${console},
    ...  name: Send order Dstore

${BROWSER}          ${ROBOT_BROWSER}
${CAPABILITIES}     ${EMPTY.join(${_tmp})}
${KEY}              mariusxcommerce:JZWeWnjviVSXJeh54zyhOqL99BshYX602FyRyJoRRbtV8jHY7F
${REMOTE_URL}       https://${KEY}@hub.lambdatest.com/wd/hub
${TIMEOUT}          5000

&{SERVER}               live=https://www.dstore.be/nl/

${VALID_USER}          alexandra@xcommerce.eu
${VALID_PASSWORD}      123456

${LOGIN_URL}            ${SERVER.${ENVIRONMENT}}login?back=my-account

${HOMEPAGE_URL}         ${SERVER.${ENVIRONMENT}}

${ACCOUNT_ORDERS}       ${SERVER.${ENVIRONMENT}}identiteit

${CHECKOUT_URL}         ${SERVER.${ENVIRONMENT}}bestelling

${PAYMENT_URL}        payment-web.sips-atos.com


#  USER _________________________________________________________
${DELAY}                0

# Submit Button From Cart
${submit_button_to_checkout}  css=div[class="text-xs-left"]

# Submit address button
${submit_address}           css=span[class="custom-radio"]

# Submit shipping method button
${submit_shipping_method}   css=div.delivery-options div:nth-child(1) div:nth-child(1) label:nth-child(1)

# Submit payment button
${submit_payment}           css=button[class="button button--primary large full"]
${submit_checkbox}          css=span[class="custom-checkbox"]
${payment_option_3}         css=label[for="payment-option-3"]


# Payment details
${visa_card_number}         4111111111111111
${card_expires_month}       css=select[id="expirydatefield-month"]> option:nth-child(4)
${card_expires_year}        css=select[id="expirydatefield-year"] > option:nth-child(8)
${card_cvv}                 123
${card_cvv_field}           css=input[id="cvvfield"]

#Details Page
${success_message_add_to_cart}          css=div[class="modal-header"]
${popup_continue_shopping_button}       css=button[class="button button--white full MB20"]
${cart_dropdown}            css=a[class="button button--header-main hidden-sm-down"]
${cart_button}              css=a[class="button button--white medium full"]
${button_add_to_cart}       css=span[class="hidden-xs-down"]

${price_zero}               €0.00

# Header
${login_button}             css=a[class="button button--header-main"]
${search_field}             css=input[class="form-control ui-autocomplete-input"]

