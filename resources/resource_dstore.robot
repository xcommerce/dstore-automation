*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library             Selenium2Library

*** Keywords ***

Open Browser To Homepage
    Open Browser                        ${HOMEPAGE_URL}   ${BROWSER}
    ...  remote_url=${REMOTE_URL}
    ...  desired_capabilities=${CAPABILITIES}
    Maximize Browser Window
    Set Selenium Speed                  ${DELAY}
    Close Cookie

Open Login Page
    ${do_not_visible_login}              ${value}=       run keyword and ignore error   Element Should Not Be Visible       css=#_desktop_user_info > div
    Run Keyword If	                    '${do_not_visible_login}' == 'PASS'    Reload Page

    Wait Until Element Is Visible       css=#_desktop_user_info > div   timeout=15

    Wait Until Element Is Visible       ${login_button}   timeout=15
    Click Element                       ${login_button}

    Location Should Be                  ${LOGIN_URL}

Complete With Valid Dates And Submit User Login
    Insert Valid Email
    Insert Valid Password
    Submit User Login And Check Location Page

Insert Valid Email
    Wait Until Element Is Visible       css=#login-form > section > div:nth-child(2) > div > input
    Input Text                          email           ${VALID_USER}

Insert Valid Password
    Wait Until Element Is Visible       css=#login-form > section > div:nth-child(3) > div > div > input
    Input Text                          password          ${VALID_PASSWORD}

Submit User Login And Check Location Page
    Click Button                        css=#login-form > footer > button
    Wait Until Element Is Visible       css=#main > div.row > div.col-xs-12.col-lg-3    timeout=15

    Location Should Be                  ${ACCOUNT_ORDERS}

Go To Home Page
    Click Image                         css=#_desktop_logo > a > img
    Location Should Be                  ${HOMEPAGE_URL}

Scroll Page To Location
    [Arguments]                          ${x_location}    ${y_location}
    Execute JavaScript                   window.scroll(${x_location},${y_location})

Close Cookie
    Wait Until Element Is Visible         css=div#gdpr_cookie_popup    timeout=15
    Click Element                         css=a[class="accept_gdrp button button--primary add-to-cart"]

    Wait Until Element Is Not Visible     css=a[class="accept_gdrp button button--primary add-to-cart"]     timeout=15

Add To Cart Button Selection
    Wait Until Element Is Visible       ${button_add_to_cart}         timeout=15
    Click Element                       ${button_add_to_cart}

#    Wait An Element After Click On Add To Cart Button
    sleep                               2s
    Condition For Out Of Stock

Condition For Out Of Stock
    ${out_ot_stock_condition}          ${value}=       run keyword and ignore error   Element Should Be Visible       css=article[class="alert alert-danger"]
    Run Keyword If	                    '${out_ot_stock_condition}' == 'PASS'    Add another product

Wait An Element After Click On Add To Cart Button
    ${wait_an_element}                  ${value}=       run keyword and ignore error   Wait Until Element Is Visible         ${popup_continue_shopping_button}    timeout=15
    Run Keyword If	                    '${wait_an_element}' == 'PASS'    Wait Until Element Is Visible         css=article[class="alert alert-danger"]    timeout=15

Add another product
    Select A Category and Random SubCategory

    Wait Until Element Is Visible       css=section#products        timeout=15

    Click Element                       css=article:nth-child(4) div.listProduct.listProduct--B2C.clearfix div.small-right div.listProduct--description > h2 > a
    Details page should be opened

    Select Variations

    Add To Cart Button Selection

Close Add To Cart Popup
    ${close_popup}                     ${value}=       run keyword and ignore error    Element Should Be Visible        ${popup_continue_shopping_button}
    Run Keyword If                     '${close_popup}' == 'PASS'        Add To Cart Popup

Add To Cart Popup
    Click Button                        ${popup_continue_shopping_button}

    Wait Until Element Is Not Visible   ${success_message_add_to_cart}        timeout=15

Select A Category and Random SubCategory
    scroll page to location             0   0
    Click Element                       css=#category-14 > a
    Wait Until Element Is Visible       css=div#subcategories        timeout=15

    ${category_random}=                 Evaluate    random.randint(1, 7)  random

    Click Element                       css=#subcategories > div > div:nth-child(5) > div > div.subcateg__links > ul > li > ul > li:nth-child(${category_random}) > a
    Condition products present in page

Condition products present in page
    ${product_list}    ${value}=       run keyword and ignore error    Element Should Not Be Visible       css=div#js-product-list
    Run Keyword If	        '${product_list}' == 'PASS'                Select A Category and Random SubCategory

Select A Random Subcategory
    Click Element                       css=#category-14 > a

    Wait Until Element Is Visible       css=div#subcategories        timeout=15

    ${subcategory_random}=                 Evaluate    random.randint(1, 7)  random

    Click Element                       css=#subcategories > div > div:nth-child(1) > div > div.subcateg__links > ul > li > ul > li:nth-child(${subcategory_random}) > a
    Wait Until Element Is Visible       css=section#products        timeout=15

Select A Product From Browse And Choose Variations
    Click Element                       css=article:nth-child(2) div.listProduct.listProduct--B2C.clearfix div.small-right div.listProduct--description > h2 > a
    Details page should be opened

    Select Variations

    Add To Cart Button Selection
    Close Add To Cart Popup

Select Variations
    Wait Until Element Is Visible       css=div.feature-section:nth-child(1) > span:nth-child(2)    timeout=15
    Click Element                       css=div.feature-section:nth-child(1) > span:nth-child(2)
    Click Element                       css=div.feature-section:nth-child(2) > span:nth-child(2)

Details page should be opened
    Wait Until Element Is Visible       css=div.product_header_left.product_header_left--with-logo    timeout=15

Search A Product And Use Autocomplete
#Open details page about product nr.2
    Click Element                       css=article:nth-child(2) div.listProduct.listProduct--B2C.clearfix div.small-right div.listProduct--description > h2 > a
    Details page should be opened

#save the name of the product
    Wait Until Element Is Visible       css=h1[class="h1"]    timeout=15

    ${PRODUCT_NAME}=                    Get Text    css=h1[class="h1"]

#Use the search field
    Wait Until Element Is Visible       ${search_field}     timeout=15
    Input Text                          ${search_field}     ${PRODUCT_NAME}

    Wait Until Element Is Visible       xpath=//*[@id="ui-id-1"]/div[2]/div/div[1]   timeout=15
    Click Element                       xpath=//*[@id="ui-id-1"]/div[2]/div/div[1]

    Select Variations

    Add To Cart Button Selection
    Close Add To Cart Popup

Search The Name Of Subcategory And Add A Product To Cart
    Wait Until Element Is Visible       css=div[class="h1 MB30"]    timeout=15

    ${SUBCATEGORY_NAME}=                    Get Text    css=div[class="h1 MB30"]

    Wait Until Element Is Visible       ${search_field}     timeout=15
    Input Text                          ${search_field}     ${SUBCATEGORY_NAME}

    Click Element                       css=button[class="button button--secondary soloIcon pull-xs-right hidden-md-down"]

    Add To Cart The Second Product

Add To Cart The Second Product
    Wait Until Element Is Visible       xpath=//*[@id="js-product-list"]/div/article[2]/div/div[2]/div[2]/h2/a    timeout=15
    Click Element                       xpath=//*[@id="js-product-list"]/div/article[2]/div/div[2]/div[2]/h2/a

    Select Variations

    Add To Cart Button Selection
    Close Add To Cart Popup

Go To Cart
    Wait Until Element Is Visible       ${cart_dropdown}        timeout=15
    Click Element                       ${cart_dropdown}

    Wait Until Element Is Visible       ${cart_button}        timeout=15
    Click Element                       ${cart_button}

    Wait Until Element Is Visible       css=div[class="cart-grid-body col-xs-12 col-lg-8"]        timeout=15

Submit Cart to Checkout
    Wait Until Element Is Visible       ${submit_button_to_checkout}   timeout=15
    Click Element                       ${submit_button_to_checkout}

    Location should be                  ${CHECKOUT_URL}

Check The Price For Products
    ${get_price1}=                      Get Text    css=div.b2c_item_cart.clearfix:nth-child(1) div:nth-child(3) div
    ${get_price2}=                      Get Text    css=div.b2c_item_cart.clearfix:nth-child(2) div:nth-child(3) div

# Product price is compared with zero value:
     Should Not Be Equal                  ${get_price1}   ${price_zero}
     Should Not Be Equal                  ${get_price2}   ${price_zero}

Select Current Address
    Wait Until Element Is Visible       ${submit_address}    timeout=15
    Click Element                       ${submit_address}

    Wait Until Element Is Visible       css=div.delivery-options        timeout=15

Select Shipping Method
    Wait Until Element Is Visible       ${submit_shipping_method}
    Click element                       ${submit_shipping_method}

    Wait Until Element Is Visible       css=div.payment-options.MB30    timeout=15

Select Payment and submit checkout
    Scroll Page To Location             0   1000

    Wait Until Element Is Visible       ${payment_option_3}       timeout=15
    Click Element                       ${payment_option_3}

    Click Element                       ${submit_checkbox}
    Click Button                        ${submit_payment}

    Wait Until Element Is Visible       css=#cardNumberField    timeout=15

Complete card number
    [Arguments]                         ${card_number}
    Input Text                          css=#cardNumberField             ${card_number}
    Unselect Frame

Complete card expires month
    Wait Until Element Is Visible       ${card_expires_month}    timeout=15
    Click Element                       ${card_expires_month}

Complete card expires year
    Wait Until Element Is Visible       ${card_expires_year}    timeout=15
    Click Element                       ${card_expires_year}

Complete card cvv
    Click Element                       ${card_cvv_field}
    [Arguments]                         ${card_cvv}
    input text                          ${card_cvv_field}                ${card_cvv}

Complete all card details in payment page
    Complete card number                ${visa_card_number}
    Complete card expires month
    Complete card expires year
    Complete card cvv                   ${card_cvv}
    capture page screenshot






